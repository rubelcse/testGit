<html>
<head>
    <title>App Name - @yield('title')</title>
</head>
<body>

@unless(Auth::check())
    You are not sign in user<br>
@endunless

@section('sidebar')
    This is the master sidebar.
@show

<div class="container">
    @yield('content')

 @includeWhen(Auth::guest(),'form');

</div>
</body>
</html>
