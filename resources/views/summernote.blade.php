{{--
<!DOCTYPE html>

<html>

<head>

    <title>Laravel 5 - Summernote Wysiwyg Editor with Image Upload Example</title>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />

    <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <!-- include summernote css/js-->

    <link href="{{asset('css/summernote.css')}}" rel="stylesheet">

    <script src="{{asset('js/summernote.js')}}"></script>

</head>

<body>

<form method="POST" action="{{ route('summernote.post') }}">

    {{ csrf_field() }}

    <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="form-group">

            <strong>Details:</strong>

            <textarea class="form-control" id="summar" name="detail"></textarea>

        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">

        <button type="submit" class="btn btn-primary">Submit</button>

    </div>

</form>

</body>

<script type="text/javascript">

    $(document).ready(function() {

        $('#summar').summernote({

            height: 300,

        });

    });

</script>

</html>--}}
        <!DOCTYPE html>

<html>

<head>

    <title>Laravel 5 - Summernote Wysiwyg Editor with Image Upload Example</title>

    <!-- dependencies (Summernote depends on Bootstrap & jQuery) -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>

</head>

<body>
<textarea id="summernote-editor" name="content">{{--{!! old('content', $content) !!}--}}</textarea>

</body>

<!-- summernote config -->
<script>
    $(document).ready(function(){

        // Define function to open filemanager window
        var lfm = function(options, cb) {
            var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
            window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
            window.SetUrl = cb;
        };

        // Define LFM summernote button
        var LFMButton = function(context) {
            var ui = $.summernote.ui;
            var button = ui.button({
                contents: '<i class="note-icon-picture"></i> ',
                tooltip: 'Insert image with filemanager',
                click: function() {

                    lfm({type: 'image', prefix: '/file-manager'}, function(url, path) {
                        context.invoke('insertImage', url);
                    });

                }
            });
            return button.render();
        };

        // Initialize summernote with LFM button in the popover button group
        // Please note that you can add this button to any other button group you'd like
        $('#summernote-editor').summernote({
            toolbar: [
                ['popovers', ['lfm']],
            ],
            buttons: {
                lfm: LFMButton
            }
        })

    });
</script>

</html>
