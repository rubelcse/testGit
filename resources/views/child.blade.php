
@extends('layouts.app')

@section('title', 'Page Title')
<p style="color: red">hello {{$name}}</p>
<div>the current timestamps is : {{time()}}</div>
<br>

@section('sidebar')

    @parent
    <p>This is appended to the master sidebar.</p>
    @unless(\Illuminate\Support\Facades\Auth::check())
        You are Not login!
    @endunless

    @isset($name)
        This vale is defiened!
    @endisset

    @empty($title)
        This is empty value.
    @endempty



@endsection

@section('content')
    <p>This is my body content.</p>
    @if(count($records) == 1)
        I Have One records!
    @elseif(count($records)> 2)
        I have two records!
    @else
        I Have no records.
    @endif
@endsection