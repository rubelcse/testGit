<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mpociot\BotMan\BotMan;

class testController extends Controller
{

    public function hello()
    {
        $botman = app('botman');
        $botman->hears('hello', function (BotMan $bot) {
            $bot->reply('Hello yourself.');
        });

// start listening
        $botman->listen();
    }


}
