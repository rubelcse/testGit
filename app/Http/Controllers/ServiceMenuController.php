<?php

namespace App\Http\Controllers;

use App\ServiceMenu;
use Illuminate\Http\Request;

class ServiceMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ServiceMenu  $serviceMenu
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceMenu $serviceMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ServiceMenu  $serviceMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceMenu $serviceMenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ServiceMenu  $serviceMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceMenu $serviceMenu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ServiceMenu  $serviceMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceMenu $serviceMenu)
    {
        //
    }
}
