<?php

namespace App\Http\Controllers;

use App\User;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;
//use App\Conversations\ExampleConversation;
use BotMan\Drivers\Facebook\Extensions\ButtonTemplate;
use BotMan\Drivers\Facebook\Extensions\ElementButton;
use BotMan\Drivers\Facebook\Extensions\GenericTemplate;
use BotMan\Drivers\Facebook\Extensions\Element;
use BotMan\Drivers\Facebook\Extensions\ReceiptTemplate;
use BotMan\Drivers\Facebook\Extensions\ReceiptSummary;
use BotMan\Drivers\Facebook\Extensions\ReceiptAdjustment;
use BotMan\Drivers\Facebook\Extensions\ReceiptElement;
use BotMan\Drivers\Facebook\Extensions\ReceiptAddress;
use BotMan\BotMan\Messages\Attachments\Location;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use SoapClient;


class BotManController extends Controller
{

    public function handle()
    {
        $botman = app('botman');
        //$requestIncomingData=$botman->getMessage()->getText();
       /* dd($requestIncomingData);*/
        dd($botman);

        // our first BotMan command
        $botman->hears('hello', function ($bot) {
            $bot->reply("Hello, I'm Hello Sajne bot!");
        });

        $botman->hears('how are you', function (BotMan $bot) {
            $bot->reply("Fine");
        });

        $botman->hears('help', function (BotMan $bot) {
            $bot->typesAndWaits(2);
            $bot->reply("Please contact me...019-711-SAjNE(72563)!");
        });

        $botman->hears('call me {name}', function (BotMan $bot,$name) {
            $bot->reply("Yeah...Please wait ".$name);
        });

        $botman->fallback(function($bot) {
            //$bot->reply('Sorry, I did not understand these commands. Here is a list of commands I understand: ...');
            $bot->reply(ButtonTemplate::create('Do you want to know more about sajne?')
                ->addButton(ElementButton::create('Tell me more')->type('postback')->payload('tellmemore'))
                ->addButton(ElementButton::create('Show Details')->url('http://demo.sajne.com/sajne-help'))
            );
        });


//latest news
        $botman->hears('latest news', function (BotMan $bot) {
            // Build message object
            $bot->typesAndWaits(2);
            $link = "http://www.radiobangla.co/api/highlighted-news";
            $data = json_decode(file_get_contents($link),true);
            foreach ($data[0]['relations'] as $d) {
                $latestNews[]=Element::create($d['title'])
                    ->image('http://radiobangla.co/uploads/news/extra-small/' . $d['main_image'][0]['image'])
                    ->addButton(ElementButton::create('read more')
                        ->url('http://radiobangla.co/news/' . $d['id'])
                    );
            }
            $bot->reply(GenericTemplate::create()
                ->addImageAspectRatio(GenericTemplate::RATIO_SQUARE)
                ->addElements($latestNews)
            );


        });

        $botman->listen();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tinker()
    {
        return view('tinker');
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function startConversation(BotMan $bot)
    {
        $bot->startConversation(new ExampleConversation());
    }
}
